<?php

namespace Tests\Unit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\AssertTrait;

class AddOrderTest extends TestCase
{
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testAddOrderOk()
	{
		$type = TYPE_CONE;
		$cost_num = 33;
		$response = $this->json('PUT', '/api/neworder', ['type' => $type, 'costumer_num' => $cost_num]);

		$response->assertStatus(200);

		$data = json_decode($response->content(), true);

		$this->assertArrayHasKey('type', $data[0]);
		$this->assertArrayHasKey('client_num', $data[0]);
		$this->assertArrayHasKey('status', $data[0]);
		$this->assertEquals($data[0]['type'], $type);
		$this->assertEquals($data[0]['client_num'], $cost_num);
	}

	public function testAddOrderNoClientNumber()
	{
		$type = TYPE_CONE;
		$response = $this->json('PUT', '/api/neworder', ['type' => $type]);

		$response->assertStatus(500);

		$data = json_decode($response->content(), true);

		$this->assertArrayHasKey('error', $data);
		$this->assertEquals($data['error'], "01: You should send a costumer_num.");
	}
}
