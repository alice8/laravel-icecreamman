<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('getpreparationtime/', 'IcecreamController@getPreparationTime');
Route::get('getallorders/', 'IcecreamController@get');
Route::get('deleteorders/', 'IcecreamController@deleteOrders');
Route::get('updateorders/', 'IcecreamController@startPreparation');

Route::put('neworder/', 'IcecreamController@createOrder');
Route::put('settimepreparation/', 'IcecreamController@setPreparationTime');
