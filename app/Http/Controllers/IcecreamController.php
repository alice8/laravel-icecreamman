<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \Cache;


class IcecreamController extends Controller
{
    /**
     * Return all the orders
     *
     * @return array
     */
    public function get(){
        if (Cache::has('total_orders')){
            $orders = Cache::get('total_orders');
            return response()->json($orders);
        }
        return 'TODO: no hay pedidos';
    }

    public function createOrder(Request $request){
        $errors = $this->checkOrderError($request);
        if($errors != 'ok'){
            return $errors;
        }

        $type = $request['type'];
        $costumer_num = $request['costumer_num'];

        if (Cache::has('total_orders')){
            $total_orders = Cache::get('total_orders');
            $status_code = $this->statusCode($total_orders, $type);

            if($status_code['preparing_2'] == true){
                $time = time();
            }else{
                $time = NULL;
            }

            array_push($total_orders, array('client_num' => $costumer_num, 'type' => $type, 'status' => $status_code['or'], 'timestamp' => time(), 'time_start_prepa' => $time));
            Cache::put('total_orders', $total_orders, 10);
        } else {
            $status_code['req'] = 200;
            $arr = array(array('client_num' => $costumer_num, 'type' => $type, 'status' => 1, 'timestamp' => time(), 'time_start_prepa' => time()));
            Cache::put('total_orders', $arr, 10);
        }

        $ord = Cache::get('total_orders');
        return response()->json($ord)->setStatusCode($status_code['req']);
    }

    public function setPreparationTime(Request $request){
        $time = $request['time'];
        Cache::put('preparation_time', $time, 100);
        $time_in_cache = Cache::get('preparation_time');

        return $time_in_cache;
    }

    public function getPreparationTime(){
        $time_in_cache = 5;
        if (!Cache::has('preparation_time')){
            Cache::put('preparation_time', $time_in_cache, 100);
        }else{
            $time_in_cache = Cache::get('preparation_time');
        }

        return $time_in_cache;
    }

    /**
     *
     * Check the status of the order.
     * If there are some icecream preparing at the moment, return request status 429 else 200
     * and return order status and a flag in true in case that the last icecream and the next are type TUB
     *
     * @return    array with
     *
     */
    private function statusCode($orders, $type){
        $preparing_2 = Cache::get('preparing_2');
        $count = count($orders);
        $flag = false;
        if($count > 0){
            if($orders[$count-1]['status'] == 2){
                $status_req = 200;
                $status_order = 1;
            }elseif($orders[$count-1]['status'] == 1 && $orders[$count-1]['type'] == TYPE_TUB && $type == TYPE_TUB && !$preparing_2){
                $status_req = 200;
                $status_order = 1;
                $flag = true;
            }else{
                $status_req = 429;
                $status_order = 0;
            }
            Cache::put('preparing_2', $flag, 10);

            return array('req' => $status_req, 'or' => $status_order, 'preparing_2' => $flag);
        }
    }

    /**
     *
     * This fucntion starts to prepare all the orders
     *
     */
    public function startPreparation(){
        $time = $this->getpreparationtime();

        if (Cache::has('total_orders')){
            $total_orders = Cache::get('total_orders');
            Cache::forget('total_orders');
            $count = count($total_orders);
            if($count > 0){
                $i = 0;
                $tub_pair = 0;
                $flag = false;
                foreach($total_orders as $order){
                    if(($i == 0 && $order['status'] == 1) || ($order['status'] == 1 && (time() - $order['time_start_prepa'] >= $time))){
                        $total_orders[$i]['status'] = 2;
                        $flag = false;
                    }else if(isset($last_status) && $last_status == 2 && $order['status'] == 0){
                        // return $last_status.''.$order['status'].''.$order['client_num'];
                        $total_orders[$i]['status'] = 1;
                        $total_orders[$i]['time_start_prepa'] = time();
                        if($order['type'] == TYPE_TUB){
                            $flag = true;
                        }
                    }else if(isset($last_status) && $last_status == 1 && $order['type'] == TYPE_TUB && $last_type == TYPE_TUB && $flag == true){
                        $total_orders[$i]['status'] = 1;
                        $total_orders[$i]['time_start_prepa'] = time();
                        $flag = false;
                    }

                    $last_type = $total_orders[$i]['type'];
                    $last_status = $total_orders[$i]['status'];
                    $i++;
                }
            }

            Cache::put('total_orders', $total_orders, 10);
            return $total_orders;
        }
    }

    public function deleteOrders(){
        Cache::forget('total_orders');
    }

    /**
     *
     * Check that the parameters that recive are ok
     *
     * @param    $request
     * @return      'ok' in case that it´s ok or and array with the details of the error
     *
     */
    private function checkOrderError($request){
        $error = false;
        $msj_error = '';
        if(!isset($request['costumer_num']) || $request['costumer_num'] == ''){
            $error = true;
            $msj_error .= '01: You should send a costumer_num.';

        }
        if(!isset($request['type']) || ($request['type'] !== TYPE_TUB && $request['type'] !== TYPE_CONE)){
            $error = true;
            $msj_error .= '02: You should send a valid type ('.TYPE_TUB.'|'.TYPE_CONE.').';
        }
        if($error){
            return response()->json(array('error'=> $msj_error))->setStatusCode(500);
        }
        return 'ok';
    }
}
