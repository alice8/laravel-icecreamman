# ICECREAM MAN

Laravel Framework 5.6.7

## Run project

Run with Docker:

```
clone project
cd /project_folder/docker
docker-compose up --build -d
```

Run in browser
```
http://localhost:8080/
```


## Technologies

In the project it is used: PHP, Laravel, Docker, PHPUnit, javascript, jQuery, HTML and CSS.

This project don´t use MySQL database, instead it use cache.

## Routes

You can see all the routes that exists in that project running:

```
php artisan route:list
```



## Important files:
```
View -> laravel-icecreamman/resources/views/icecream.blade.php
Controller -> laravel-icecreamman/app/Http/Controllers/IcecreamController.php
API routes -> laravel-icecreamman/routes/api.php
View routes -> laravel-icecreamman/routes/web.php
Test PHPUnit -> laravel-icecreamman/tests/Unit/AddOrderTest.php
Constants -> laravel-icecreamman/config/constants.php
```


## API request url:


http://localhost:8080/api/deleteorders    (GET)  Para Eliminar todos los pedidos
http://localhost:8080/api/neworder         (PUT)  Enviando  'type' => (TUB|CONE)  , 'costumer_num' => (numeric)
http://localhost:8080/api/updateorders   (GET)  Para comenzar a realizar los pedidos
http://localhost:8080/api/settimepreparation  (PUT) Enviando  'time' => 5 (número de segundos para preparar el pedido)
http://localhost:8080/api/getallorders  (GET)  Para obtener todos los pedidos
http://localhost:8080/api/getpreparationtime   (GET)   Para obtener el numero de segundos que hay en ese momento guardado

## PHPUnit test

There are two unit tests (the path of the file is project_folder/tests/Unit/AddOrderTest.php) one of them tests the response of the API when creating a new request and the other one tests the error in case of not receiving the data 'costumer_num.'

Run Unit Test:

```
cd /project_folder/
./vendor/bin/phpunit
```



