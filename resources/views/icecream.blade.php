<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
		<title>The icecream truck</title>
		<link id="favicon" rel="shortcut icon" type="image/png" href="images/cone_32.png"/>
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

		<!-- Styles -->
		<style>
			html, body {
				background-color: #fff;
				color: #636b6f;
				font-family: 'Raleway', sans-serif;
				font-weight: 100;
				height: 100vh;
				margin: 0;
			}

			.full-height {
				height: 100vh;
			}

			.flex-center {
				align-items: center;
				display: flex;
				justify-content: center;
			}

			.position-ref {
				position: relative;
			}

			.top-right {
				position: absolute;
				right: 10px;
				top: 18px;
			}

			.content {
				text-align: center;
			}

			.title {
				font-size: 84px;
			}

			.links > a {
				color: #636b6f;
				padding: 0 25px;
				font-size: 12px;
				font-weight: 600;
				letter-spacing: .1rem;
				text-decoration: none;
				text-transform: uppercase;
			}

			.m-b-md {
				margin-bottom: 30px;
			}

			.img_ice{
				height: 1.5em;
				margin-bottom: -.3em;
			}

			#table_all_orders{
				margin-left: 13%;
				width: 70%;
			}

			.imglogo{
				height: 2em;
				margin-bottom: -.5em;
			}

			#span_time{
				color: #33cc33;
			}

			img[src=""] {
			   display: none;
			}

			.img_loading{
				width: 1em;
			}
		</style>
	</head>
	<body>
		<div class="flex-center position-ref">
			<div class="content">
				<div class="title m-b-md">
					<img class="imglogo" alt='logo' src='images/icecream_Logo.png'/> IceCream Truck
				</div>
				<hr>
				<div id = "newOrder" class="m-b-md">
				   <span>Make a new order</span>
					<select id="sel_icecream">
					  <option value="TUB">Tub</option>
					  <option value="CONE">Cone</option>
					</select>
				   <input id="new_order" type="button" value="order">
				</div>
				<div id="allOrder" class="m-b-md">
					<table id="table_all_orders">
						<thead>
							<tr>
								<th>Client number:</th>
								<th>Type icecream</th>
								<th>Img</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="m-b-md">
					Set preparation time: <input id="prepa_time" type="number">
					<input id="but_save_time" type="button" value="Save"><br/>
					<span id="span_time">Changes saved successfully</span>
				</div>
				<div class="m-b-md">
					<input id="but_delete_orders" type="button" value="Delete orders">
					<input id="but_update_orders" type="button" value="Start to prepare"><br/>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			window.onload = function() {
				show_list_all_orders();
				$('#but_hide_list').hide();
				$('#span_time').hide();
				$('#but_delete_orders').hide();
				get_preparation_time();
			};


			$('#but_delete_orders').click(function () {
				delete_orders();
			});

			$('#new_order').click(function () {
				make_new_order();
			});

			$('#but_hide_list').click(function () {
				hide_list();
			});

			$('#but_update_orders').click(function () {
				var time = $("#prepa_time").val();
				time = parseInt(time) * 1000;
				var total_orders = $("#table_all_orders > tbody > tr").length;
				if(total_orders > 0){
					var i = 0;
					do {
						(function(i){
							setTimeout(function(){
								if($('.tick').length < total_orders){
									update_orders();
								}
							}, time * i);
						})(i);
						i++;
					}while (i < total_orders);


				}
			});

			$("#but_show_list").click(function () {
				$('#allOrder').show();
				$('#but_hide_list').show();
				$('#but_show_list').hide();
			});

			$('#but_save_time').click(function () {
				set_preparation_time();
			});

			function update_orders(){
				console.log('entra en update orders 2***')
				var xhr = $.ajax({
					url: "/api/updateorders",
					type: 'GET',
					dataType: 'json'
				}).done(function(data){
					if(xhr.status == 200){
						console.log(data);
						print_table(data);
					}
				});
			}

			function get_img_icecream(type_ice){
				var img_icecream = '';
				switch(type_ice) {
					case 'CONE':
					case 'CON':
						img_icecream = 'cone_32.png';
						break;
					case 'TUB':
						img_icecream = 'tube_32.png';
						break;
					default:
						img_icecream = '';
				}
				return img_icecream;
			}

			function hide_list(){
				$('#but_show_list').show();
				$('#allOrder').hide();
				$('#but_hide_list').hide();
			}

			function make_new_order(){
				console.log("New order type: " + $('#sel_icecream').val() +" client: "+ $("#table_all_orders > tbody > tr").length+1 );
				var xhr = $.ajax({
					url: "/api/neworder",
					type: 'PUT',
					dataType: 'json',
					data: {"type": $('#sel_icecream').val(), "costumer_num": $("#table_all_orders > tbody > tr").length+1},
				}).done(function(data){
					console.log(data);
					if(xhr.status == 200){
						print_table(data);
					}
				}).fail(function (data) {
					if(xhr.status == 429 ){
						print_table(data['responseJSON']);
					}

				});
			}

			function print_table(data){
				$('#table_all_orders tbody').html("");
				if(data.length > 0){
					var html_list = '';
					for (var i = 0; i < data.length; i++) {
						var img_ice = get_img_icecream(data[i]['type']);
						html_list += "<tr><td>"+data[i]['client_num']+"</td>";
						html_list += "<td>"+data[i]['type']+"</td>";
						html_list += "<td><img class='img_ice' alt='ice' src='images/"+img_ice+"'></td>";
						if(data[i]['status'] == 2){
							html_list += "<td><img id="+data[i]['client_num']+" class='img_ice tick' alt='ice' src='images/tick.png'></td></tr>";
						}else if(data[i]['status'] == 1){
							html_list += "<td><img id="+data[i]['client_num']+" class='img_loading' alt='ice' src='images/loading.gif'></td></tr>";
						}else{
							html_list += "<td><img id="+data[i]['client_num']+" class='img_ice' alt='ice' src=''></td></tr>";
						}
					}
					$('#table_all_orders tbody').html(html_list);
					$('#but_delete_orders').show();
				}
			}

			function set_preparation_time(){
				var time = $('#prepa_time').val();
					if(time != "" && time > 0){
					var xhr = $.ajax({
						url: "/api/settimepreparation",
						type: 'PUT',
						dataType: 'json',
						data: {"time": time},
					}).done(function(data){
						if(xhr.status == 200){
							$('#span_time').show();
							$('#span_time').fadeOut(5000);
							console.log("Preparation time Setted to: " + data);
						}
					}).fail(function (data) {
						console.log('There is some unexpected error while processing your request. Please try again after some time.');
					});
				}
			}

			function get_preparation_time(){
				var time = 5;
				var xhr = $.ajax({
					url: "/api/getpreparationtime",
					type: 'GET',
				}).done(function(data){
					if(xhr.status == 200){
						time = data;
					}
				}).fail(function (data) {
					console.log('There is some unexpected error while processing your request. Please try again after some time.');
				});
				$("#prepa_time").val(time);
			}

			function delete_orders(){
				var xhr = $.ajax({
					type: "GET",
					url: "/api/deleteorders",
					}).done(function(data){
						if(xhr.status == 200){
							$('#table_all_orders tbody').html("");
						}
				}).fail(function (data) {
					console.log('delete_orders: There is some unexpected error while processing your request. Please try again after some time.');
				});
			}

			function show_list_all_orders(){
				var time = parseInt(time);

				var xhr = $.ajax({
					type: "GET",
					url: "/api/getallorders",
					}).done(function(data){
						if(xhr.status == 200 && typeof data == 'object'){
							console.log(data);
							print_table(data);
						}
				}).fail(function (data) {
					console.log('show_list_all_orders: There is some unexpected error while processing your request. Please try again after some time.');
				});
			}

		</script>
	</body>
</html>
